# Personify
Lien Web: https://trainznation.tk/download/12
Version Actuel: 0.0.1-alpha0

Ce module permet la génération des noeuds d'un fichier de configuration pour trainz (config.txt).

## Prérequis

- Serveur Web en ligne ou local (laragon ou wampserver)
- php7.3

## Installation

- `git clone https://gitlab.com/trainznation/application/personify.git`
- ``cd personify``
- ``composer install``
- ``cp .env.example .env``
- ``php artisan key:generate``
- sur votre navigateur: https://votre-dossier.ndl

## Mise à jours

- ``git pull``
- ``git checkout v0.0.1alpha-0``
- ``composer install``
- ``php artisan clear``
- ``php artisan config:clear``
- ``php artisan cache:clear``
- ``php artisan route:clear``
- ``php artisan clear:clear``

![](https://nsa40.casimages.com/img/2020/12/05/201205042409535541.png)
