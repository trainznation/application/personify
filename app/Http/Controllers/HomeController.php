<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function generate(Request $request)
    {
        for($i = 0; $i <= $request->get('nb'); $i++) {
            $array[] = [
                "key" => $i,
                "name" => $request->get('name'),
                "kind"          => "attachment",
                "att"           => "a.".$request->get('name'),
                "default_mesh"  => $request->get('kuid')
            ];
        }

        $collection = $array;
        return $collection;
    }
}
