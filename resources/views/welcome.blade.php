@extends('layouts.layout')

@section('content')
    <div class="m-5">
        <div class="jumbotron">
            <h1 class="display-4">Bienvenue</h1>
            <hr class="my-4">
            <p>Le principe de cette utilitaire est de générer les différents noeuds du fichier de config des assets de trainz.<br>Pour le moment seul les points du kind <code>attachment</code> sont pris en charge</p>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Génération des noeuds `effects`</h3>
                <form action="/" id="form" class="mt-1" method="post">
                    <div class="form-group">
                        <label>KUID</label>
                        <input type="text" name="kuid" class="form-control" placeholder="kuid:XXXXXXX:XXX" data-toggle="tooltip" title="Entrez le kuid de l'objet sous format: kuid:XXXXXX:XXX">
                    </div>

                    <div class="form-group">
                        <label>Nom du noeud</label>
                        <input type="text" name="name" class="form-control" placeholder="banc" data-toggle="tooltip" title="Entrez le nom principal du point. exemple: si votre point est a.banc0001 alors entrez banc">
                    </div>

                    <div class="form-group">
                        <label>Nombre de noeud</label>
                        <input type="text" name="nb" class="form-control" placeholder="5" data-toggle="tooltip" title="Entrez le nombre de noeud de vos points, il s'agit du nombre -1.">
                    </div>

                    <button type="submit" id="btnSubmit" class="btn btn-primary">Afficher la configuration</button>
                </form>
            </div>
        </div>
    </div>
    <div id="result" style="display: none;">
        <div class="card">
            <div class="card-body">
                <code id="resultCode"></code>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">

        (function($) {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            let result = document.querySelector("#result");
            let code = document.querySelector("#resultCode")
            $("#form").on('submit', (e) => {
                e.preventDefault()
                let form = $("#form");
                let btn = $("#btnSubmit")
                let url = form.attr('action')
                let data = form.serializeArray()

                btn.html(`<i class="fa fa-spinner fa-spin"></i> Chargement...`)

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    success: (data) => {
                        code.innerHTML = '';
                        btn.html(`Envoyer`)
                        Array.from(data).forEach(item => {
                            console.log(item.key)
                            code.innerHTML += `
                                ${(item.key >= 0 && item.key <= 9) ? item.name + '00' + item.key : (item.key >= 100) ? item.name + item.key : item.name + '0' + item.key}<br>
                                {<br>
                                    kind            &emsp;&emsp;&emsp;&emsp;"${item.kind}"<br>
                                    name            &emsp;&emsp;&emsp;&emsp;"${item.name+item.key}"<br>
                                    att             &emsp;&emsp;&emsp;&emsp;"${(item.key >= 0 && item.key <= 9) ? item.att + '00' + item.key : (item.key >= 100) ? item.att + item.key : item.att + '0' + item.key}"<br>
                                    default-mesh    &emsp;&emsp;&emsp;&emsp;&lt;${item.default_mesh}&gt;<br>
                                }<br><br>
                            `
                        })
                        result.style.display = 'block';
                    },
                    error: (error) => {
                        console.error(error);
                        btn.html(`Envoyer`)
                    }
                })
            });
        })(jQuery)
    </script>
@endsection
